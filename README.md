# Lintang

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://pypi.org/project/lintang/)

This module is personally developed as a documentation of all functions or methods or classes that are most often used by it's developers.

### Release

  - Date convertion from Gregorian to Javanese-Hijri Calendar
  - Calculate distance with Heversine formula
  - Factorial numbers
  - Permutation and combination
  - Read data from .csv file

### Installation

Lintang requires [Python 3.6](https://www.python.org/download/releases/3.0/) +.

Install the module with pip.

```sh
$ pip install lintang
```

##
### Documentation:

##### Haversine Formula

![enter image description here](https://upload.wikimedia.org/wikipedia/commons/3/38/Law-of-haversines.svg)

The haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes. Important in navigation, it is a special case of a more general formula in spherical trigonometry, the law of haversines, that relates the sides and angles of spherical triangles.

The first table of haversines in English was published by James Andrew in 1805, but Florian Cajori credits an earlier use by José de Mendoza y Ríos in 1801. The term haversine was coined in 1835 by James Inman.

These names follow from the fact that they are customarily written in terms of the haversine function, given by haversin(θ) = sin2(
θ
/
2
). The formulas could equally be written in terms of any multiple of the haversine, such as the older versine function (twice the haversine). Prior to the advent of computers, the elimination of division and multiplication by factors of two proved convenient enough that tables of haversine values and logarithms were included in 19th and early 20th century navigation and trigonometric texts. These days, the haversine form is also convenient in that it has no coefficient in front of the sin2 function.


```
from lintang.calc import Haversine
```

Usage:
```text
from lintang.calc import Haversine

lon1 = -103.548851
lat1 = 32.0004311
lon2 = -103.6041946
lat2 = 33.374939

distance = Haversine.get_distance(lat1, lon1, lat2, lon2)

print(distance)
```
result:
```text
152.96923374692167
```

get_distance parameters:
```text
lon1 : <float> longitude domain
lat1 : <float> latitude domain

lon2 : <float> longitude destination
lat2 : <float> latitude destination

type : <string> "kilometers" or "miles", (default in kilometers)

```
##
##### Factorial Number
In mathematics, the factorial of a positive integer n, denoted by n!, is the product of all positive integers less than or equal to n. For example,

5!=5x4x3x2x1=120

The value of 0! is 1, according to the convention for an empty product.

The factorial operation is encountered in many areas of mathematics, notably in combinatorics, algebra, and mathematical analysis. Its most basic use counts the possible distinct sequences -- the permutations -- of n distinct objects: there are n!

```text
from lintang.calc import factorial
```
Usage:
```text
number = factorial(5)

print(number)
```
result:
```text
120
```

##
##### Permutation and Combination

- Permutation

In mathematics, permutation is the act of arranging the members of a set into a sequence or order, or, if the set is already ordered, rearranging (reordering) its elements—a process called permuting. Permutations differ from combinations, which are selections of some members of a set regardless of order. 

For example, written as tuples, there are six permutations of the set {1,2,3}, namely: (1,2,3), (1,3,2), (2,1,3), (2,3,1), (3,1,2), and (3,2,1). These are all the possible orderings of this three-element set. Anagrams of words whose letters are different are also permutations: the letters are already ordered in the original word, and the anagram is a reordering of the letters. The study of permutations of finite sets is an important topic in the fields of combinatorics and group theory.
```text
from lintang.calc import permutation
```
Usage:
```text
number =  permutation(7, 3)

print(number)
```
result:
```text
120
```
- Combination

A combination is a selection of items from a collection, such that (unlike permutations) the order of selection does not matter. For example, given three fruits, say an apple, an orange and a pear, there are three combinations of two that can be drawn from this set: an apple and a pear; an apple and an orange; or a pear and an orange. More formally, a k-combination of a set S is a subset of k distinct elements of S. If the set has n elements, the number of k-combinations is equal to the binomial coefficient

```text
from lintang.calc import combination
```
Usage:
```text
number =  combination(7, 3)

print(number)
```
result:
```text
35
```

##
##### Neptu: Date Conversion to Javanese Calendar
Convert date to Javanese calendar
```text
from lintang.calendar import Neptu
```
Usage:
```text
date = Neptu.convert_date(15, 5, 2019)

print(date.get('pasaran'))
```
result:
```text
'Kliwon'
```
convert_date parameters:
```text
convert_date(day, month, year)

day     : <int> day
month   : <int> month    
year    : <int> year
```

convert_date result:
```text
 {
  'day': <int day>,
  'month': <int month>,
  'month_name': <string month_name>,
  'day_name': <string day_name>,
  'pasaran': <string pasaran>,
  'year': <int year>
}
```

##
##### Polaris: Read data from csv file

```text
from lintang.core import Polaris
```
Usage:
```text
a = Polaris.read_csv("/path/to/file.csv")

print(a.data)
```
result:
```text
[{'age': 19, 'name': 'Nikhil'}, {'age': 20, 'name': 'Nandini'}, {'age': 20, 'name': 'Manjeet'}]
```
Order by field name:
```text
a = Polaris.read_csv("/path/to/file.csv")

a.order_by('field_name').data

# or 

a.order_by('field_name', descending=True).data

```

License
----

Copyright (c) 2018 The Python Packaging Authority

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.